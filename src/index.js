import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';

import generateStore from './redux/store'

// import configureStore from './redux/store';
// const store = configureStore();

const store = generateStore()

ReactDOM.render(<App store={store} />, document.getElementById('root'));

