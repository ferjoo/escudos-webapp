import axios from 'axios'

const DATA_TOP = 'DATA_TOP_POKEMONES';
const DATA_CATEGORIAS = 'DATA_CATEGORIAS';
const DATA_PRODUCTOS = 'DATA_PRODUCTOS';
const CATEGORIA_SELECCIONADO = 'CATEGORIA_SELECCIONADO';
const CARRITO_COMPRAS = 'CARRITO_COMPRAS';

export const dataInicial = {
    data_top: [],
    data_categorias: [],
    data_productos: [],
    categoria_seleccionado: 0,
    carrito_compras: [],
};

const setDataTop = data_top => ({
    type: DATA_TOP,
    data_top,
});

const setDataCategorias = data_categorias => ({
    type: DATA_CATEGORIAS,
    data_categorias,
});
const setDataProductos = data_productos => ({
    type: DATA_PRODUCTOS,
    data_productos,
});

const setCategoriaSeleccionado = categoria_seleccionado => ({
    type: CATEGORIA_SELECCIONADO,
    categoria_seleccionado,
});

const setCarritoCompras = carrito_compras => ({
    type: CARRITO_COMPRAS,
    carrito_compras,
});


// reducer
export default function produtos(state = dataInicial, action) {
    switch (action.type) {
        case DATA_TOP:
            return { ...state, data_top: action.data_top }
        case DATA_CATEGORIAS:
            return { ...state, data_categorias: action.data_categorias }
        case DATA_PRODUCTOS:
            return { ...state, data_productos: action.data_productos }
        case CATEGORIA_SELECCIONADO:
            return { ...state, categoria_seleccionado: action.categoria_seleccionado }
        case CARRITO_COMPRAS:
            return { ...state, carrito_compras: action.carrito_compras }
        default:
            return state
    }
}

// acciones
export const eliminarCarritoCompras = (id) => (dispatch, getState) => {
    let carrito = [...getState().productos.carrito_compras]
    carrito.forEach(elemento => {
        if (elemento.id == id) {
            elemento.cantidad = elemento.cantidad - 1
        }
    });
    const result = carrito.filter(producto => producto.cantidad > 0);
    dispatch(setCarritoCompras(result))
}

export const agregarCarritoCompras = (item) => (dispatch, getState) => {
    let carrito = [...getState().productos.carrito_compras]
    let ingresar_nuevo = true
    carrito.forEach(elemento => {
        if (elemento.id == item.id) {
            elemento.cantidad = elemento.cantidad + 1
            ingresar_nuevo = false
        }
    });

    if (ingresar_nuevo) {
        let nuevo = { ...item }
        nuevo.cantidad = 1
        carrito.push(nuevo)
    }
    dispatch(setCarritoCompras(carrito))
}

// acciones
export const getTopProductos = () => async (dispatch, getState) => {
    try {
        const res = await axios.get(`http://localhost:3030/top`)
        dispatch(setDataTop(res.data))
    } catch (error) {
        console.log(error)
    }
}

// acciones
export const getCategorias = () => async (dispatch, getState) => {
    try {
        const res = await axios.get(`http://localhost:3030/categorias`)
        dispatch(setDataCategorias(res.data))
    } catch (error) {
        console.log(error)
    }
}

// acciones
export const getProductos = (categoria_seleccionado = 0) => async (dispatch, getState) => {
    try {
        let res = null

        if (categoria_seleccionado === 0) {
            res = await axios.get(`http://localhost:3030/productos`)
        } else {
            res = await axios.get(`http://localhost:3030/productos?categoria=${categoria_seleccionado}`)
        }

        dispatch(setDataProductos(res.data))
        dispatch(setCategoriaSeleccionado(categoria_seleccionado))
    } catch (error) {
        console.log(error)
    }
}