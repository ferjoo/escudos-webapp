import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';
import Productos from './components/Productos'
import ProtectedRouter from './ProtectedRouter'
import Tamano from './components/Tamano'

const App = ({ store }) => (
	<Provider store={store}>

		<Router>

			<Switch>
				{/* <Route exact path="/" component={Home} />
					<Route path="/results" component={Results} />
					<ProtectedRouter path="/movie/:movieId">
						<MovieDetail />
					</ProtectedRouter> */}
				{/* <ProtectedRouter path="/" component={Productos} /> */}
				<ProtectedRouter exact path="/" component={Productos} />
				<ProtectedRouter path="/tamano" component={Tamano} />

			</Switch>
		</Router>
	</Provider>
);

App.propTypes = {
	store: PropTypes.object.isRequired
};

export default App;
