import React from 'react'
import Footer from './Footer';
import NavBar from './NavBar';
const index = ({ children }) => {
  return (

    <div className="container-fluit bg-blue" >
      <NavBar />
      <main className="container">
        {children}
      </main>
      <Footer />
    </div>
  )
}


export default index;
