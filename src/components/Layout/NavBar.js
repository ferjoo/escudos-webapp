import React from 'react'

const NavBar = () => {
  return (
    <nav className="navbar navbar-dark bg-dark" aria-label="First navbar example">
      <div className="container">
        <a className="navbar-brand bold" href="#">LOS ESCUDOS</a>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarsExample01">
          <ul className="navbar-nav me-auto mb-2">
            <li className="nav-item">
              <a className="nav-link active" aria-current="page" href="#">Home</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Inicio de sesión</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Carrito de compras</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default NavBar
