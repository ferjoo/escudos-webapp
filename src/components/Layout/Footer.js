import React from 'react'
const Footer = () => {
  return (
    <footer className="bg-light text-center text-lg-start footer ">
      <div className="text-center p-3" style={{ backgroundColor: "rgba(0, 0, 0, 0.2)" }}>
        © 2021 Los escudos:
    <a className="text-dark" href="https://mdbootstrap.com/" style={{marginLeft: '5px'}}>La mejor comida de Guatemala</a>
      </div>
    </footer>
  )
}

export default Footer
