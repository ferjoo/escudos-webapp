import React, { useEffect } from 'react'

import { useDispatch, useSelector } from "react-redux";
import { agregarCarritoCompras } from "../../redux/productos";

const Carousel = () => {
  const dispatch = useDispatch();
  const data_productos = useSelector((store) => store.productos.data_productos);


  return (
    <div id="carouselExampleDark" className="carousel carousel-dark slide h-25" data-bs-ride="carousel">
      <h3 className="my-3">Productos recomendados:</h3>
      <div className="carousel-inner text-center  h-25 my-auto">
        {data_productos.map((item, index) =>
        (<div key={item.id} className={`carousel-item text-center ${index == 1 ? "active" : ""} `} data-bs-interval="2500" onClick={() => {
          dispatch(agregarCarritoCompras(item))
        }}>
          <img src={item.imagen} className="d-block text-center mx-auto carousel-media" />
          <div className="carousel-caption d-none d-md-block">
            <h5 className="bg-light border border-3 rounded-3 bold d-inline px-4 py-2 carrouselLabel">{item.nombre}</h5>
          </div>
        </div>))}
      </div>
      <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Previous</span>
      </button>
      <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
        <span className="carousel-control-next-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Next</span>
      </button>
    </div>
  )
}

export default Carousel
