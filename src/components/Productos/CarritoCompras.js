import React, { useState, useEffect } from 'react'

import { useDispatch, useSelector } from "react-redux";
import { eliminarCarritoCompras } from "../../redux/productos";


const CarritoCompras = () => {
  const dispatch = useDispatch();
  const [VerCarrito, setVerCarrito] = useState(false)

  const carrito_compras = useSelector((store) => store.productos.carrito_compras);
  var total = carrito_compras.reduce((sum, value) => {
    const suma = value.cantidad * value.precio
    return suma + sum
  }, 0);

  useEffect(() => {
    if (carrito_compras.length == 0) {
      setVerCarrito(false)
    }
  }, [carrito_compras])

  return (
    <>
      <div className={`modal ${carrito_compras.length > 0 && !VerCarrito ? "d-block" : ""}`} style={{ width: "auto", top: "50vh", right: 0, left: "auto" }}>
        <button type="button" className="btn btn-secondary" onClick={() => {
          setVerCarrito(true)
        }}>
          Productos<span className="badge bg-white text-black-50">{carrito_compras.length}</span>
        </button>
      </div>

      <div className={`modal end-0 ${carrito_compras.length > 0 && VerCarrito ? "d-block" : ""}`} style={{ height: 'auto', maxHeight: "92%", maxWidth: 350, right: 0, left: "auto" }} id="exampleModalScrollable" aria-labelledby="exampleModalScrollableTitle" aria-modal="true" role="dialog">
        <div className="modal-dialog modal-dialog-scrollable me-0" style={{ height: "100%", }}>
          <div className="modal-content" style={{ maxHeight: '90vh', }}>
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalScrollableTitle">Mis productos </h5>
            </div>
            <div className="modal-body">
              {carrito_compras.map((item, index) => (
                <div className="d-flex my-2" key={index}>
                  <span className="fw-regular my-auto  ps-0 pe-2 py-3 btn"
                    onClick={() => {
                      dispatch(eliminarCarritoCompras(item.id))
                    }}
                  >X </span>
                  <div className="w-50 ">
                    <img src={item.imagen} className="img-thumbnail img-fluid"></img>
                  </div>
                  <div className="d-flex flex-column flex-1">
                    <span className="fw-bold">{item.nombre}</span>
                    <span className="fw-bold">Precio: <span className="fw-normal">Q{item.precio}</span> </span>
                    <span className="fw-bold">Cantidad: <span className="fw-normal">{item.cantidad}</span> </span>
                    <span className="fw-bold">Subtotal: <span className="fw-normal">Q{item.precio * item.cantidad}</span> </span>
                  </div>
                </div>

              ))}
              <div class="d-flex">
                <span className="fw-bold ms-4 mt-3">Total a pagar: <span className="fw-normal">Q{total}</span> </span>
              </div>

            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-primary">Pedir producto</button>
              <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" onClick={() => {
                setVerCarrito(false)
              }} >Seguir Comprando</button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default CarritoCompras
