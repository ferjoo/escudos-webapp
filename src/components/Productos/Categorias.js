import React, { useEffect } from 'react'
import { useDispatch, useSelector } from "react-redux";
import { getCategorias, getProductos } from "../../redux/productos";
import './style.css'
const Categorias = () => {
  const dispatch = useDispatch();
  const data_categorias = useSelector((store) => store.productos.data_categorias);
  const categoria_seleccionado = useSelector((store) => store.productos.categoria_seleccionado);

  useEffect(() => {
    dispatch(getCategorias())
  }, [])

  return (
    <div className="my-4">
      <h3 className="my-3">Categorias:</h3>
      {data_categorias.map((item) => (
        <button
          key={item.id}
          type="button"
          className={`m-2 px- py-1 btn ${categoria_seleccionado == item.id ? "btn-info" : "btn-warning"}`}
          onClick={() => {
            dispatch(getProductos(item.id))
          }}
        >{item.nombre}</button>
      ))
      }
    </div >
  )
}

export default Categorias
