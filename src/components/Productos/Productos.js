import React, { useEffect } from 'react'
import { useDispatch, useSelector } from "react-redux";
import { getProductos, agregarCarritoCompras } from "../../redux/productos";

const Productos = () => {
  const dispatch = useDispatch();
  const data_productos = useSelector((store) => store.productos.data_productos);
  useEffect(() => {
    dispatch(getProductos())
  }, [])

  return (
    <div className="my-4">
      <h3 className="my-3">Productos:</h3>


      <section className="border p-4 justify-content-center mb-4">
        <div className="row text-center" id="basic-example-data">

          {data_productos.map((item, index) => (
            <div
              className="col-md-4 mt-3 basic-example-item"
              key={item.id}
            >
              <div className="card shadow-2">
                <img
                  src={item.imagen}
                  className="card-img-top"
                />

                <div className="card-body">
                  <h5 className="card-title">{item.nombre}</h5>
                  <p className="card-text">Q{item.precio}</p>
                  <button onClick={() => {
                    dispatch(agregarCarritoCompras(item))
                  }} className="btn btn-primary ripple-surface">Añadir</button>
                </div>
              </div>
            </div>
          ))}


        </div>
      </section>
    </div>
  )
}

export default Productos
