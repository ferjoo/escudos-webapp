import React from 'react';
import { withRouter } from "react-router";
import Carousel from '../Productos/Carousel';
import CarritoCompras from './CarritoCompras';
import Categorias from './Categorias';
import Productos from './Productos'




const MovieResult = () => {

  return (
    <>
      <Categorias />
      <Carousel />
      <Productos />
      <CarritoCompras />
    </>

  );
}

export default MovieResult;
