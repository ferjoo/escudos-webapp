import React from 'react';
import { useState, useEffect } from 'react';


const Tamano = () => {
  const size = useWindowSize();
  return (
    <>
      <h1>holaa</h1>
      <h2>
        {size.width}px / {size.height}px</h2>
    </>

  );
}

export default Tamano;

function useWindowSize() {
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
  });
  useEffect(() => {
    function handleResize() {
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    }
    window.addEventListener("resize", handleResize);
    handleResize();

    return () => window.removeEventListener("resize", handleResize);
  }, []);
  return windowSize;
}