
import React from 'react';
import Layout from './components/Layout'
import { Route, Redirect } from 'react-router-dom';

const ProtectedRouter = ({ children, ...restoDePropiedades }) => {

	if (true) {
		return (
			<Layout>
				<Route {...restoDePropiedades}>{children}</Route>
			</Layout>
		)
	} else {
		return <Redirect to="/iniciar-sesion" />
	}
}

export default ProtectedRouter;